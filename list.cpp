///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler - EE 205 - Spr 2021
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all lists
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   16_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include<cassert>

#include "list.hpp"

//#define DEBUG

using namespace std;

const bool DoubleLinkedList::empty() const { 
   return ( (head == nullptr) );
#ifdef DEBUG
   validate();
#endif 
}

void  DoubleLinkedList::push_front( Node* newNode ) {
   if( newNode == nullptr ) {
#ifdef DEBUG
      cout << "nothing added." << endl;   // @note if DEBUG lets user know nothing happened, returns
#endif                                    // @note else silently returns
      return;
   }
   if( tail == nullptr ){ tail = newNode; }
   newNode->next = head;
   newNode->prev = nullptr;
   if( !empty() ){ head->prev = newNode; }   // link the current front to newNode
   head = newNode;

   numberOFnodes++;
}

Node* DoubleLinkedList::pop_front() {
   if( head == nullptr ) {
#ifdef DEBUG
      cout << "nothing to pop." << endl;
#endif
      return nullptr;   // @note when DEBUG not defined, silently return without pop
   }
   Node* oldHead = head;
   if( head == tail ){  //if there is only 1 node in list
      head = nullptr;
      tail = nullptr;
      numberOFnodes--;
      return oldHead;
   }
   head->next->prev = oldHead->prev;   // basically setting it to null
   head = head->next;
   oldHead->next = nullptr;

   numberOFnodes--;
   return oldHead;
}

void DoubleLinkedList::push_back( Node* newNode ) {
   if( newNode == nullptr ) {
#ifdef DEBUG
      cout << "nothing added." << endl;   // @note if DEBUG lets user know nothing happened, returns
#endif                                    // @note else just silently returns
      return;
   }
   newNode->prev = tail;
   if( !empty() ){
      tail->next = newNode;
   }else{                                 // else the list is empty and head needs to be updated
      head = newNode;
   }
   newNode->next = nullptr;
   tail = newNode;

   numberOFnodes++;
}

Node* DoubleLinkedList::pop_back() {
   if( tail == nullptr ) {
#ifdef DEBUG
      cout << "nothing to pop." << endl;
#endif
      return nullptr;   // @note when DEBUG not defined, silently return without pop
   }
   Node* oldTail = tail;
   if( head == tail ){  // @note if there is only 1 node in list
      head = nullptr;
      tail = nullptr;
      numberOFnodes--;
      return oldTail;
   }
   tail->prev->next = oldTail->next;
   tail = tail->prev;

   numberOFnodes--;
   return oldTail;
}

void DoubleLinkedList::insert_after( Node* currentNode, Node* newNode ){
#ifdef DEBUG
   validate();    // check if list is healthy before inserting
#endif
   if( newNode == nullptr ){
#ifdef DEBUG   // @note when DEBUG not defined, silently return without inserting 
      cout << "nothing to insert." << endl;
#endif
      return;
   }
   if( currentNode == nullptr && head == nullptr ){   // the list is empty
      push_front( newNode );     // this situation is already handled in push_front()
      return;
   }
   assert( !empty() );
   assert( isIn( currentNode ) );
   assert( !isIn( newNode ) );
   if( tail != currentNode ){
      newNode->prev           = currentNode;
      newNode->next           = currentNode->next;
      currentNode->next->prev = newNode;
      currentNode->next       = newNode;
      
      numberOFnodes++;
   } else{        // we are inserting at the end of lisr OR there is only 1 node in list
      push_back( newNode );
   }
#ifdef DEBUG
   validate();    // check if list is healthy after inserting
#endif
}

void DoubleLinkedList::insert_before( Node* currentNode, Node* newNode ){
#ifdef DEBUG
   validate();    // check if list is healthy before inserting
#endif
   if( newNode == nullptr ){
#ifdef DEBUG      // @note when DEBUG not defined, silently return without inserting 
      cout << "nothing to insert." << endl;
#endif
      return;
   }
   if( currentNode == nullptr && head == nullptr ){ // the list is empty
      push_front( newNode );  // this situation is already handled in push_front()
      return;
   }
   assert( !empty() );
   assert( isIn( currentNode ) );
   assert( !isIn( newNode ) );
   if( head != currentNode ){
      newNode->next           = currentNode;
      newNode->prev           = currentNode->prev;
      currentNode->prev->next = newNode;
      currentNode->prev       = newNode;

      numberOFnodes++;
   } else{        // inserting at the front of lisr OR there is only 1 node in list
      push_front( newNode );
   }
#ifdef DEBUG
   validate();    // check if list is healthy before inserting
#endif
}  

Node* DoubleLinkedList::get_first() const { return head; }

Node* DoubleLinkedList::get_next( const Node* currentNode ) const {
   assert( currentNode != nullptr );   //@note get_next(): if an illegal access is tried stops program
   if( !isIn( currentNode ) ){
      cout << "ERROR: node is not in the list." << endl;
   }
#ifdef DEBUG
   validate();
#endif
   return currentNode->next;
}

Node* DoubleLinkedList::get_prev( const Node* currentNode ) const {
   assert( currentNode != nullptr );   //@note get_prev(): if an illegal access is tried stops program
   if( !isIn( currentNode ) ){
      cout << "ERROR: node is not in the list." << endl;
   }
#ifdef DEBUG
   validate();
#endif
   return currentNode->prev;
}

Node* DoubleLinkedList::get_last() const { return tail; }

const bool DoubleLinkedList::isIn( const Node* aNode ) const {
   Node* currentNode = head;

   while( currentNode != nullptr ){
      if( aNode == currentNode ){
         return true;
      }
      currentNode = currentNode->next;
   }
   return false; // aNode wasn't found in the list
}


const bool DoubleLinkedList::isSorted() const {
   if( numberOFnodes <= 1 ) // if the list is empty or only has one item...
      return true;

   for( Node* i = head; i->next != nullptr; i = i->next ){
      if( *i > *i->next )  // if current object is > next object
         return false;
   }
   return true;
}

void DoubleLinkedList::insertionSort() {
#ifdef DEBUG
   validate();
#endif
   if( isSorted() ){ return; }   // if it's sorted there is nothing to do!
   
   for( Node* i = head; i->next != nullptr; i = i->next ){              // outer loop
      Node* minNode = i;
      
      for( Node* iter = i->next; iter != nullptr; iter = iter->next ){  // inner loop
         if( *minNode > *iter ){
            minNode = iter;
         }
      }// now minNode has smallest value out of all unsorted Nodes
      swap( i, minNode );
      i = minNode;   // This fixes i so that it will properly iterate
   }
#ifdef DEBUG
   validate();
   dump();
#endif
   assert( isSorted() );   // the list should now be sorted
}

void DoubleLinkedList::swap( Node* node1, Node* node2 ) {
#ifdef DEBUG
   validate();
#endif 
   if( node1 == node2 ) { return; }    // if swap with itself do nothing
   assert( isIn( node1 ) );            // make sure parameters are valid
   assert( isIn( node2 ) );
   assert( node1 != nullptr );         // make sure they aren't null
   assert( node2 != nullptr );

   // check if node1 is before node2, if not change their naming order
   Node* firstNode = nullptr;
   Node* secondNode = nullptr;
   Node* currentNode = head;
   while( currentNode != nullptr ){
      if( node1 == currentNode ){   // they are in order, no need to switch names
         firstNode = node1;
         secondNode = node2;
         break;
      } else if( node2 == currentNode ) { // they are out of order, switch names
         firstNode = node2;
         secondNode = node1;
         break;
      }
      currentNode = currentNode->next;
   }

   Node* firstNode_oldNext  = firstNode->next;      // some temporary variables to hold values
   Node* firstNode_oldPrev  = firstNode->prev;
   Node* secondNode_oldNext = secondNode->next;
   Node* secondNode_oldPrev = secondNode->prev;

   if ( numberOFnodes == (2) ) {                                           // if there are only 2 nodes in the list
      head = secondNode;
      tail = firstNode;

      firstNode->next  = secondNode_oldNext;  // switch firstNode and secondNode's links
      firstNode->prev  = secondNode;
      secondNode->next = firstNode;
      secondNode->prev = firstNode_oldPrev;
   } else if( (firstNode->next == secondNode) && (firstNode == head) ){    // if they are right next to each other
      secondNode->next->prev = firstNode;                                  // and the firstNode is the first in the list
      head                   = secondNode;
      
      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode;
      secondNode->next = firstNode;
      secondNode->prev = firstNode_oldPrev;
   } else if( (firstNode->next == secondNode) && (secondNode == tail) ){   // if they are right next to each other
      firstNode->prev->next = secondNode;                                  // and the secondNode is the last in the list
      tail                   = firstNode;
      
      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode;
      secondNode->next = firstNode;
      secondNode->prev = firstNode_oldPrev;
   } else if( firstNode->next == secondNode ){                             // if they are right next to each other
      secondNode->next->prev = firstNode;    // switch the "outer" nodes' links
      firstNode->prev->next  = secondNode;
      
      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode;
      secondNode->next = firstNode;
      secondNode->prev = firstNode_oldPrev;
   } else if ( (firstNode == head) && (secondNode == tail) ) {             // if first is head AND second is tail and things are between
      secondNode->prev->next = firstNode;
      firstNode->next->prev  = secondNode;
      head                   = secondNode;
      tail                   = firstNode;

      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode_oldPrev;
      secondNode->next = firstNode_oldNext;
      secondNode->prev = firstNode_oldPrev;
   } else if ( firstNode == head ){                                        // if first node is the head 
      secondNode->next->prev = firstNode;
      secondNode->prev->next = firstNode;
      firstNode->next->prev  = secondNode;
      head                   = secondNode;

      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode_oldPrev;
      secondNode->next = firstNode_oldNext;
      secondNode->prev = firstNode_oldPrev;
   } else if ( secondNode == tail ){                                       // if second node is the tail
      secondNode->prev->next = firstNode;
      firstNode->next->prev  = secondNode;
      firstNode->prev->next  = secondNode;
      tail                   = firstNode;
      
      firstNode->next  = secondNode_oldNext;
      firstNode->prev  = secondNode_oldPrev;
      secondNode->next = firstNode_oldNext;
      secondNode->prev = firstNode_oldPrev;
   } else {                                                                // if it's nothing above then it's the normal case
      secondNode->next->prev = firstNode;    // switch the "outer" nodes' links
      secondNode->prev->next = firstNode;
      firstNode->next->prev  = secondNode;
      firstNode->prev->next  = secondNode;

      firstNode->next  = secondNode_oldNext;  // switch firstNode and secondNode's links
      firstNode->prev  = secondNode_oldPrev;
      secondNode->next = firstNode_oldNext;
      secondNode->prev = firstNode_oldPrev;
   }
#ifdef DEBUG
   validate();
#endif 
}

void DoubleLinkedList::dump() const {
   cout << "------------------------------ DoubleLinkedList ------------------------------" << endl;
   cout << "      head = [" << head << "]    tail = [" << tail << "]" << endl;
   for( Node* currentNode = head; currentNode != nullptr; currentNode = currentNode->next ){
      cout << "   ";
      currentNode->dump();
   }
   cout << "------------------------------------------------------------------------------" << endl;
}
