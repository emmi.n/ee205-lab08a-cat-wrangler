///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file mytest.cpp 
/// @version 1.0
///
/// A unit test file for the cat wrangler program
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   04_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "list.hpp"
#include "node.hpp"

using namespace std;

int main(){
   Node node1, node2, node3, node4;
   DoubleLinkedList list;  //Instantiate a DoubleLinkedList

   cout << "Welcome to my test" << endl;
   cout << "Is it empty: "<< boolalpha << list.empty() << endl;
   cout << "------------------------------------------------" << endl;

   list.push_front( &node4 );
   list.push_front( &node3 );
   list.push_front( &node2 );
   list.push_front( &node1 );
   list.push_front( nullptr );
   cout << "------------------------------------------------" << endl;
   
   cout << "Is it empty: "<< boolalpha << list.empty() << endl;
   cout << "Number of Nodes: "<< list.size() << endl;
   cout << "First Node: "<< list.get_first() << endl;
   cout << "Last Node: "<< list.get_last() << endl;
   cout << "Node after First: "<< list.get_next( list.get_first() ) << endl;
   cout << "Node after Node1: "<< list.get_next( &node1 ) << endl;

   cout << "------------------------------------------------" << endl;
   list.pop_front();
   list.pop_front();
   cout << "Number of Nodes: "<< list.size() << endl;
   list.pop_front();
   list.pop_front();
   list.pop_front();
   
   cout << "------------------------------------------------" << endl;
   cout << "Is it empty: "<< boolalpha << list.empty() << endl;
   cout << "Number of Nodes: "<< list.size() << endl;
   cout << "First Node: "<< list.get_first() << endl;
   cout << "Last Node: "<< list.get_last() << endl;
   //cout << "Node after First: "<< list.get_next( list.get_first() ) << endl;

   cout << "------------------------------------------------" << endl;
   list.push_back( &node4 );
   list.push_back( &node3 );
   list.push_back( &node2 );
   list.push_back( &node1 );
   list.push_back( nullptr );
   cout << "------------------------------------------------" << endl;
   cout << "Is it empty: "<< boolalpha << list.empty() << endl;
   cout << "Number of Nodes: "<< list.size() << endl;
   cout << "Node before Last: "<< list.get_prev( list.get_last() ) << endl;
   cout << "Node before Node1: "<< list.get_prev( &node1 ) << endl;

   cout << "------------------------------------------------" << endl;
   list.pop_back();
   list.pop_back();
   cout << "Number of Nodes: "<< list.size() << endl;
   list.pop_back();
   list.pop_back();
   list.pop_back();

   cout << "this is the end of the test. here's your <cake>" << endl;
   return 0;
}
