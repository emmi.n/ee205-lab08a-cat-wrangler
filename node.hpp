///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 1.0
///
/// A generic Node class.  May be used as a base class for a Doubly Linked List
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   @todo 09_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Node {
friend class DoubleLinkedList;

protected:
	Node* next = nullptr;
	Node* prev = nullptr;

public:
	virtual bool operator>(const Node& r);
   void dump() const;

}; // class Node
