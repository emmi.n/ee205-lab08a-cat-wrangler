///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file mytest2.cpp 
/// @version 1.0
///
/// A second unit test file for the cat wrangler program
/// (this incorporates the use of cats)
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   16_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <cassert>

#include "list.hpp"
#include "node.hpp"
#include "cat.hpp"

using namespace std;

int main(){
   cout << "Welcome to my test 2" << endl;
   
   Cat::initNames();
   DoubleLinkedList catList;

   cout << "Is it empty: " << boolalpha << catList.empty() << endl;
   cout << "Is it valid: " << boolalpha << catList.validate() << endl;
   cout << "Number of Nodes: " << catList.size() << endl;

   for( int i = 0; i < 100; i++){
      catList.push_front( Cat::makeCat() );  

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }

   for( int i = 0; i < 99; i++){
      catList.pop_front();  

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }
   catList.pop_front();  
   catList.pop_front();  

   for( int i = 0; i < 100; i++){
      catList.push_back( Cat::makeCat() );  

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }
   
   cout << "-----------------------------------------------" << endl;
   cout << "Is it empty: "<< boolalpha << catList.empty() << endl;
   cout << "Number of Nodes: "<< catList.size() << endl;
   cout << "First Node: "<< catList.get_first() << endl;
   cout << "Last Node: "<< catList.get_last() << endl;
   cout << "Node after First: "<< catList.get_next( catList.get_first() ) << endl;
   cout << "Node after Last: "<< catList.get_next( catList.get_last() ) << endl;
   cout << "-----------------------------------------------" << endl;

   for( int i = 0; i < 99; i++){
      catList.pop_back();  

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }
   catList.pop_back();  
   catList.pop_back(); 

   Cat* newCat;
   Cat* oldCat = Cat::makeCat();
   catList.insert_after( nullptr, (Node*)oldCat );

   for( int i = 0; i < 100; i++){
      newCat = Cat::makeCat();
      catList.insert_after( (Node*)oldCat, (Node*)newCat );

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
      
      //oldCat = newCat;  // the new cat becomes the old cat
   }

   Node aRandomNode;
   Node* aRandomNodePointer = &aRandomNode;
   
   catList.get_next( aRandomNodePointer );
   catList.get_prev( aRandomNodePointer );

   Cat* aCat = Cat::makeCat();
   //catList.insert_after( aRandomNodePointer, (Node*)aCat );  // test for not in list
   catList.insert_after( (Node*)oldCat, (Node*)aCat );         // uses last cat inserted
   catList.insert_after( (Node*)oldCat, nullptr );             // test when nothing to insert
   //catList.insert_after( (Node*)oldCat, (Node*)aCat );       // test when already in list
   
   cout << "Is it valid: "<< boolalpha << catList.validate() << endl;

   oldCat = (Cat*)catList.get_prev( catList.get_last() );    // get second to last cat

   for( int i = 0; i < 100; i++){
      newCat = Cat::makeCat();
      catList.insert_before( (Node*)oldCat, (Node*)newCat );

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
      
      oldCat = newCat;  // the new cat becomes the old cat
   }
   aCat = Cat::makeCat();
   //catList.insert_before( aRandomNodePointer, (Node*)aCat ); // test for not in list
   catList.insert_before( catList.get_first(), (Node*)aCat );  // test when insert before first cat
   catList.insert_before( (Node*)oldCat, nullptr );            // test when nothing to insert
   //catList.insert_before( catList.get_last(), (Node*)aCat ); // test when already in list

   for( int i = 0; i < 200; i++){
      catList.pop_front();  

      assert( !catList.empty() );
      assert( catList.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }

   cout << " done with catList now start a new list" << endl;
   cout << "----------------------------------------------" << endl;
   DoubleLinkedList list;

   cout << "Is it empty: " << boolalpha << list.empty() << endl;
   cout << "Is it valid: " << boolalpha << list.validate() << endl;
   cout << "Number of Nodes: " << list.size() << endl;

   for( int i = 0; i < 50; i++){
      list.push_front( Cat::makeCat() );  

      assert( !list.empty() );
      assert( list.validate() );
      //cout << "Number of Nodes: " << catList.size() << endl;
   }
   //list.dump();

   Node* node1 = nullptr;
   Node* node2 = nullptr;
   Node* stopAtThisNode = list.get_prev( list.get_prev( list.get_prev( list.get_prev( list.get_last()))));
  
   // test passing in 1 nullptr, and a ptr not in the list
   //list.swap( node1, list.get_first() );
   //list.swap( list.get_first(), catList.get_first() );

   // test "regular" swap
   for ( Node* currentNode = list.get_first()      ;  // for loop iterator
         currentNode != stopAtThisNode             ;  // loop condition
         currentNode = list.get_next( currentNode) ) {
      node1 = list.get_next( currentNode );
      node2 = list.get_next( list.get_next( node1 )  );

      //cout << "node1: " << node1 << "    node2: " << node2 << endl;
      list.swap( node1, node2 );
      assert( list.validate() );
   }
   //list.dump();

   // test "regular" swap, but nodes out of order
   for ( Node* currentNode = list.get_first()      ;  // for loop iterator
         currentNode != stopAtThisNode             ;  // loop condition
         currentNode = list.get_next( currentNode) ) {
      node1 = list.get_next( currentNode );
      node2 = list.get_next( list.get_next( node1 )  );

      //cout << "node1: " << node1 << "    node2: " << node2 << endl;
      list.swap( node2, node1 ); // now passing them in diffrent places
      assert( list.validate() );
   }
   //list.dump();

   // test swap with itself
   for ( Node* currentNode = list.get_first()      ;  // for loop iterator
         currentNode != stopAtThisNode             ;  // loop condition
         currentNode = list.get_next( currentNode) ) {
      node1 = list.get_next( currentNode );
      node2 = list.get_next( list.get_next( node1 )  );

      //cout << "node1: " << node1 << "    node2: " << node2 << endl;
      list.swap( node1, node1 );
      list.swap( node2, node2 );
      assert( list.validate() );
   }
   //list.dump();
   
   // test next to each other swap
   stopAtThisNode = list.get_prev( list.get_prev( list.get_prev( list.get_last())));
   //list.dump();
   for ( Node* currentNode = list.get_first()      ;  // for loop iterator
         currentNode != stopAtThisNode             ;  // loop condition
         currentNode = list.get_next( currentNode ) ) {
      node1 = currentNode;
      node2 = list.get_next( node1 );

      //cout << "node1: " << node1 << "    node2: " << node2 << endl;
      list.swap( node1, node2 );
      assert( list.validate() );
   }
   //list.dump();

   // test next to each other swap, but passing them in in "backwards" order
   stopAtThisNode = list.get_prev( list.get_prev( list.get_prev( list.get_last())));
   //list.dump();
   for ( Node* currentNode = list.get_first()      ;  // for loop iterator
         currentNode != stopAtThisNode             ;  // loop condition
         currentNode = list.get_next( currentNode ) ) {
      node1 = currentNode;
      node2 = list.get_next( node1 );

      //cout << "node1: " << node1 << "    node2: " << node2 << endl;
      list.swap( node2, node1 ); // flip the order
      assert( list.validate() );
   }
   //list.dump();

   // test swap head with tail
   node1 = list.get_first();
   node2 = list.get_last();
   for ( int i = 0; i < 30; i++ ) {
      list.swap( node1, node2 );
      assert( list.validate() );
      //cout << "first node: " << list.get_first() << "    last node: " << list.get_last() << endl;
   }

   // test swap one head other random
   node1 = list.get_first();
   node2 = list.get_next( list.get_next( node1 )  );
   for ( int i = 0; i < 30; i++ ) {
      //cout << "first node: " << list.get_first() << endl;
      list.swap( node1, node2 );
      assert( list.validate() );
      node2 = list.get_next( node1 );
      node1 = list.get_first();
   }
   //list.dump();

   // test swap one head other next to it
   node1 = list.get_first();
   node2 = list.get_next( node1 );
   for ( int i = 0; i < 30; i++ ) {
      list.swap( node1, node2 );
      assert( list.validate() );
      node1 = list.get_first();
      node2 = list.get_next( node1 );
   }
   //list.dump();

   // test swap one tail other random
   node1 = list.get_last();
   node2 = list.get_next( list.get_next( list.get_first() )  );
   for ( int i = 0; i < 25; i++ ) {
      //cout << "node1: " << node1 << "  node2: " << node2 << endl;
      list.swap( node1, node2 );
      assert( list.validate() );
      node2 = list.get_next( node1 );
      node1 = list.get_last();
   }

   // test swap one tail other next to it
   node1 = list.get_last();
   node2 = list.get_prev( list.get_last() );
   for ( int i = 0; i < 25; i++ ) {
      //cout << "node1: " << node1 << "  node2: " << node2 << endl;
      list.swap( node1, node2 );
      assert( list.validate() );
      node1 = list.get_last();
      node2 = list.get_prev( list.get_last() );
   }
  
   //cout << "Number of Nodes: " << list.size() << endl;
   // cutting down the list to 2 nodes
   for( int i = 0; i < 48; i++){
      list.pop_front();  

      assert( !list.empty() );
      assert( list.validate() );
      //cout << "Number of Nodes: " << list.size() << endl;
   }
   //list.dump();
   
   // test swap with only 2 nodes
   node1 = list.get_last();
   node2 = list.get_first();
   list.swap( node1, node2 );
   assert( list.validate() );


   cout << " done with list now start a new list: shortList" << endl;
   cout << "-----------------------------------------------" << endl;
   DoubleLinkedList shortList;

   cout << "Is it empty: " << boolalpha << shortList.empty() << endl;
   cout << "Is it valid: " << boolalpha << shortList.validate() << endl;
   cout << "Number of Nodes: " << shortList.size() << endl;

   for( int i = 0; i < 10; i++){
      newCat = Cat::makeCat();
      cout << newCat->getInfo() << endl;
      shortList.push_front( newCat );  

      assert( !shortList.empty() );
      assert( shortList.validate() );
      //cout << "Number of Nodes: " << shortList.size() << endl;
   }
   shortList.dump();

   cout << "Is it sorted: " << boolalpha << shortList.isSorted() << endl;
   shortList.insertionSort();
   
   for( Cat* cat = (Cat*)shortList.get_last() ; cat != nullptr ; cat = (Cat*)shortList.get_prev( cat )) {
		cout << cat->getInfo() << endl;
	}
   
   cout << "Is it sorted: " << boolalpha << shortList.isSorted() << endl;

   cout << "this is the end of the test 2." << endl;
   return 0;
}
