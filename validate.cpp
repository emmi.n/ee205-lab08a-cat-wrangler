///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler - EE 205 - Spr 2021
///
/// @file validate.cpp
/// @version 1.0
///
/// Exports validate() for use in list.cpp's DoubleLinkedList
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   06_APR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include<cassert>

#include "list.hpp"


using namespace std;

const bool DoubleLinkedList::validate() const {
   // @note validation of emptiness:
   // @note if found invalid sytem will stop process and throw flag
   if( head == nullptr ){
      assert( tail == nullptr );
      assert( numberOFnodes == 0 );
   } else {
      assert( tail != nullptr );
      assert( numberOFnodes != 0 );
   }
   
   if( tail == nullptr ){
      assert( head == nullptr );
      assert( numberOFnodes == 0 );
   } else {
      assert( head != nullptr );
      assert( numberOFnodes != 0 );
   }
   // @note validation of a non-empty list
   if( (head != nullptr) && (tail == head) ) {
      assert( numberOFnodes == 1 );
   }
   // @note forward count through list
   // @note checks both counter and links
   unsigned int forwardCount = 0;
   Node* currentNode = head;
   while( currentNode != nullptr ){
      forwardCount++;
      if(currentNode->next != nullptr ){
         assert( currentNode->next->prev == currentNode );
      }
      currentNode = currentNode->next;
   }
   assert( numberOFnodes == forwardCount );
   
   // @note backward count through list
   // @note checks both counter and links
   unsigned int backwardCount = 0;
   currentNode = tail;
   while( currentNode != nullptr ){
      backwardCount++;
      if(currentNode->prev != nullptr ){
         assert( currentNode->prev->next == currentNode );
      }
      currentNode = currentNode->prev;
   }
   assert( numberOFnodes == backwardCount );

   return true; // all tests passed
}

