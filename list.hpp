///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler - EE 205 - Spr 2021
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all lists
///
/// @author EmilyPham <emilyn3@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   14_APR_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once
#include "node.hpp"


class DoubleLinkedList {
private:
   unsigned int numberOFnodes = 0; // an internal count of the # of nodes in a list

protected:
	Node* head = nullptr;
	Node* tail = nullptr;

public:
   const bool validate() const;        // a validate function that checks a list for any errors
   const bool isIn( const Node* aNode ) const;
   void dump() const;
   const bool isSorted() const;     // this function depends on Node's < operator
   void insertionSort();            // this function depends on Node's < operator

public:
   const bool empty() const;           // @note grouped methods by functionality
   inline unsigned int size() const;

   void  push_front( Node* newNode );
   Node* pop_front();
   void  push_back( Node* newNode );
   Node* pop_back();
   void insert_after( Node* currentNode, Node* newNode );
   void insert_before( Node* currentNode, Node* newNode );
   
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
   Node* get_prev( const Node* currentNode ) const;
   Node* get_last() const;

   void swap( Node* node1, Node* node2 ); // a generic swap
};

inline unsigned int DoubleLinkedList::size() const {
      validate();                      //@note added a validate line 43
      return numberOFnodes;
   }
