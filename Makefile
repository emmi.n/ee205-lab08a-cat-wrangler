###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author EmilyPham <emilyn3@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   14_APR_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

all: main test mytest mytest2 queueSim

main.o:  main.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

node.o: node.cpp node.hpp
	$(CXX) -c $(CXXFLAGS) $<

list.o: list.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

validate.o: validate.cpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

test.o:  test.cpp node.hpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

mytest.o:  mytest.cpp node.hpp list.hpp
	$(CXX) -c $(CXXFLAGS) $<

mytest2.o:  mytest2.cpp node.hpp list.hpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

main: *.hpp main.o cat.o list.o node.o validate.o
	$(CXX) -o $@ main.o cat.o list.o node.o validate.o

test: *.hpp test.o cat.o node.o list.o validate.o
	$(CXX) -o $@ test.o cat.o node.o list.o validate.o

mytest: *.hpp mytest.o cat.o node.o list.o validate.o
	$(CXX) -o $@ mytest.o cat.o node.o list.o validate.o

mytest2: *.hpp mytest2.o cat.o node.o list.o validate.o
	$(CXX) -o $@ mytest2.o cat.o node.o list.o validate.o

queueSim.o: queueSim.cpp *.hpp
	$(CXX) -c $(CXXFLAGS) $<

queueSim: *.hpp queueSim.o list.o node.o validate.o
	$(CXX) -o $@ queueSim.o list.o node.o validate.o

clean:
	rm -f *.o main queueSim test mytest mytest2
